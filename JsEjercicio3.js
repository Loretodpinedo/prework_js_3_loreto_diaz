//1. Solicite al usuario su número de DNI y su letra. Debes realizar las preguntas por separado, es decir que tendrá que introducir dos datos, no uno. 
//Pista: para solicitar un dato al usuario puedes usar la función [prompt](https://developer.mozilla.org/es/docs/Web/API/Window/prompt)
//2. En primer lugar (y en una sola instrucción) se debe comprobar si el número es menor que `0` o mayor que `99999999`. 
//Si ese es el caso, se muestra un mensaje al usuario indicando que el número proporcionado no es válido y el programa no muestra más mensajes.
//3. Si el número es válido (paso), debes comprobar si la letra indicada por el usuario es correcta, para ello sigue el método explicado anteriormente. 
//Deberás indicar al usuario el resultado de la validación, imprimiendo por pantalla "DNI Válido" o "DNI Incorrecto".


var numeroDni = Number(window.prompt('numero dni'));
var letraDni = prompt("¿Cual es la letra de tu DNI?");
var letrasPosiblesDni = ['T', 'R', 'w', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E', 'T'];

function comprobarNumeroDni (numeroDniComprobar){
    
    if (numeroDniComprobar < 0 || numeroDniComprobar > 99999999){
        alert("El numero no es correcto");
    }
}

function calcularLetraDni(numeroDni){
    
   var restoLetra = numeroDni%23;
   for (var i = 0; i<= letrasPosiblesDni.length; i++ ){
       letraDniCalculada= letrasPosiblesDni[restoLetra];
       return letraDniCalculada;
        }
}


function comprobarLetraDni(letraDniCalculada, letraDni){
    
    if (letraDniCalculada === letraDni){
        alert("DNI válido");
    }else{
        alert("DNI incorrecto");
    }
}

comprobarNumeroDni(numeroDni);
calcularLetraDni(numeroDni);
comprobarLetraDni(letraDni, letraDniCalculada);


